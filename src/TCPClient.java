
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.SecureRandom;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * This class is the sender
 * @author Shabbir
 *
 */
public class TCPClient { 
	
	public static int numberOfTests=100;
	public static int minimumMessageLen = 100; //length in bytes
	public static int messageGrowth = 1000; //growth factor in bytes
	
	//global variable used to hold random data
	public static String data; 
	
	public static void main(String args[]) throws IOException
	{
		
		//create csv writer
		CSVWriter writer = new CSVWriter(new FileWriter("WifiTests.csv"), ',');
		
		int i=0;
		//send varying length messages to server and time the results
		for(i=0;i<numberOfTests;i++){

			//generate the data before sending
			data = GenerateData(minimumMessageLen+i*messageGrowth);
			
			//measure sending time
			boolean result;
			long start = System.nanoTime();
			result = sendMessage();
			long stop = System.nanoTime();
			
			System.out.println("elapsed: "+ ((stop-start)/1000000));
			
			//construct Entry
			String Entry[] = new String[3];
			Entry[0] = ""+i;
			Entry[1] = ""+(minimumMessageLen+i*messageGrowth);
			Entry[2] = ""+((stop-start)/1000000);
			
			//Write to csv
			writer.writeNext(Entry);
			
			//check if tests are completing
			if(result){
				continue;
			}else{
				break;
			}
		}
		
		//close writer
		writer.close();
		
		//echo status of experiment
		if(i==numberOfTests){
			System.out.println("Successful Tests");
		}
		else{
			System.out.println("Failed Tests");
		}
	}
	
	/**
	 * This method sends a file to the client.
	 * @param MsgLen
	 * @return
	 */
	public static boolean sendMessage() 
	{
		Socket s = null; 
		try{ 
			int serverPort = 8080;
			  String ip = "142.157.73.39"; 
			  
			  s = new Socket(ip, serverPort); 
			  DataInputStream input = new DataInputStream( s.getInputStream()); 
			  DataOutputStream output = new DataOutputStream( s.getOutputStream()); 
		  
			  //Step 1 send length
			  System.out.println("Length"+ data.length());
			  output.writeInt(data.length());
			  //Step 2 send data
			  System.out.println("Sending.......");
			  output.writeBytes(data); // UTF is a string encoding
			  
			  //Step 1 read length
			  int nb = input.readInt();
			  if(nb == data.length()){
				  System.out.println("Confirmation Received");
				  return true;
			  }else{
				  return false;
			  }
		}
		catch (UnknownHostException e){ 
			System.out.println("Sock:"+e.getMessage());}
		catch (EOFException e){
			System.out.println("EOF:"+e.getMessage()); }
		catch (IOException e){
			System.out.println("IO:"+e.getMessage());} 
		finally {
			  if(s!=null) 
				  try {
					  s.close();
				  } 
				  catch (IOException e) {/*close failed*/}
		}
		
		return false;
  }
	
	/**
	 * This method generate a string of specified length
	 * @param dataLength specified length
	 * @return string of length dataLength
	 */
	public static String GenerateData(int dataLength){
				
		char[] text = new char[dataLength];
	    for (int i = 0; i < dataLength; i++)
	    {
	        text[i] = 'a';
	    }
	    return new String(text);
		
	}
}

