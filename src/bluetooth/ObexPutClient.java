package bluetooth;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.microedition.io.Connector;
import javax.obex.ClientSession;
import javax.obex.HeaderSet;
import javax.obex.Operation;
import javax.obex.ResponseCodes;

import au.com.bytecode.opencsv.CSVWriter;

public class ObexPutClient {
	
	public static int numberOfTests=100;
	public static int minimumMessageLen = 100; //length in bytes
	public static int messageGrowth = 1000; //growth factor in bytes
	
	//global variable used to hold random data
	public static String data; 

	public static void main(String[] args) throws IOException, InterruptedException {
		//create csv writer
		CSVWriter writer = new CSVWriter(new FileWriter("bluetoothTests.csv"), ',');
		
		int i=0;
		//send varying length messages to server and time the results
		for(i=0;i<numberOfTests;i++){

			//generate the data before sending
			data = GenerateData(minimumMessageLen+i*messageGrowth);
			
			//measure sending time
			boolean result;
			long start = System.nanoTime();
			result = sendMessage();
			long stop = System.nanoTime();
			
			System.out.println("elapsed: "+ ((stop-start)/1000000));
			
			//construct Entry
			String Entry[] = new String[3];
			Entry[0] = ""+i;
			Entry[1] = ""+(minimumMessageLen+i*messageGrowth);
			Entry[2] = ""+((stop-start)/1000000);
			
			//Write to csv
			writer.writeNext(Entry);
			
			//check if tests are completing
			if(result){
				continue;
			}else{
				break;
			}
		}
		
		//close writer
		writer.close();
		
		//echo status of experiment
		if(i==numberOfTests){
			System.out.println("Successful Tests");
		}
		else{
			System.out.println("Failed Tests");
		}
	}
	
	/**
	 * This method sends a file to the client.
	 * @param MsgLen
	 * @return
	 * @throws  
	 */
	public static boolean sendMessage()  
	{
		try{
			String serverURL = "btgoep://A41731969B9E:1";
	       
	        System.out.println("Connecting to " + serverURL);
	
	        ClientSession clientSession = (ClientSession) Connector.open(serverURL);
	        HeaderSet hsConnectReply = clientSession.connect(null);
	        if (hsConnectReply.getResponseCode() != ResponseCodes.OBEX_HTTP_OK) {
	            System.out.println("Failed to connect");
	            return false;
	        }

	        HeaderSet hsOperation = clientSession.createHeaderSet();
	        hsOperation.setHeader(HeaderSet.NAME, "Hello.txt");
	        hsOperation.setHeader(HeaderSet.TYPE, "text");
	
	        //Create PUT Operation
	        Operation putOperation = clientSession.put(hsOperation);
	
	        System.out.println("Data Length: "+ data.length());
	        // Send some text to server
	        byte bydata[] = data.getBytes("iso-8859-1");
	        OutputStream os = putOperation.openOutputStream();
	        os.write(bydata);
	        os.close();
	
	        putOperation.close();
	
	        clientSession.disconnect(null);
	
	        clientSession.close();
	        
        	return true;
		}
		catch(Exception e){
			return false;
		}
  }
	
	/**
	 * This method generate a string of specified length
	 * @param dataLength specified length
	 * @return string of length dataLength
	 */
	public static String GenerateData(int dataLength){
				
		char[] text = new char[dataLength];
	    for (int i = 0; i < dataLength; i++)
	    {
	        text[i] = 'a';
	    }
	    return new String(text);
		
	}
	
    public static void maina(String[] args) throws IOException, InterruptedException {

        String serverURL = null; // = "btgoep://0019639C4007:6";
        if ((args != null) && (args.length > 0)) {
            serverURL = args[0];
        }
        if (serverURL == null) {
            String[] searchArgs = null;
            // Connect to OBEXPutServer from examples
            searchArgs = new String[] { "11111111111111111111111111111123" };
            ServicesSearch.main(searchArgs);
            if (ServicesSearch.serviceFound.size() == 0) {
                System.out.println("OBEX service not found");
                return;
            }
            // Select the first service found	
            serverURL = (String)ServicesSearch.serviceFound.elementAt(0);
        }

        System.out.println("Connecting to " + serverURL);

        //start time
        
        ClientSession clientSession = (ClientSession) Connector.open(serverURL);
        HeaderSet hsConnectReply = clientSession.connect(null);
        if (hsConnectReply.getResponseCode() != ResponseCodes.OBEX_HTTP_OK) {
            System.out.println("Failed to connect");
            return;
        }

        HeaderSet hsOperation = clientSession.createHeaderSet();
        hsOperation.setHeader(HeaderSet.NAME, "Hello.txt");
        hsOperation.setHeader(HeaderSet.TYPE, "text");

        //Create PUT Operation
        Operation putOperation = clientSession.put(hsOperation);

        // Send some text to server
        byte data[] = "Hello ryan!".getBytes("iso-8859-1");
        OutputStream os = putOperation.openOutputStream();
        os.write(data);
        os.close();

        putOperation.close();

        clientSession.disconnect(null);

        clientSession.close();
    }
}