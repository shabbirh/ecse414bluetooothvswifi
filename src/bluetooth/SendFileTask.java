/*
 * SendFileTask.java
 *
 * Copyright (C) 2008 Tommi Laukkanen
 * http://www.substanceofcode.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package bluetooth;

import java.io.OutputStream;
import javax.microedition.io.Connection;
import javax.microedition.io.Connector;
import javax.obex.ClientSession;
import javax.obex.HeaderSet;
import javax.obex.Operation;

/**
 *
 * @author Tommi Laukkanen (tlaukkanen at gmail dot com)
 */
public class SendFileTask implements Runnable {

    private String btConnectionURL;
    private byte[] file;
    private String filename;

    public SendFileTask(String url, byte[] file, String filename) {
        this.btConnectionURL = url;
        this.file = file;
        this.filename = filename;
    }

    public void run() {

        try {
            Connection connection = Connector.open(btConnectionURL);
            // connection obtained

            // now, let's create a session and a headerset objects
            ClientSession cs = (ClientSession) connection;
            HeaderSet hs = cs.createHeaderSet();

            // now let's send the connect header
            cs.connect(hs);

            hs.setHeader(HeaderSet.NAME, filename);
            hs.setHeader(HeaderSet.TYPE, "image/jpeg");
            hs.setHeader(HeaderSet.LENGTH, new Long(file.length));

            Operation putOperation = cs.put(hs);

            OutputStream outputStream = putOperation.openOutputStream();
            outputStream.write(file);
            // file push complete

            outputStream.close();
            putOperation.close();

            cs.disconnect(null);

            connection.close();
        } catch (Exception e) {
            System.err.println("Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }
}

