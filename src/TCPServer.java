
import java.net.*; 
import java.io.*; 

/** 
 * This class receives fileas
 * @author Shabbir
 *
 */
public class TCPServer { 
  public static void main (String args[]) 
  { 
	try{ 
			int serverPort = 8080; 
			ServerSocket listenSocket = new ServerSocket(serverPort); 
	  
			System.out.println("server start listening... ... ...");
		
			while(true) { 
				Socket clientSocket = listenSocket.accept(); 
				Connection c = new Connection(clientSocket); 
			} 
	} 
	catch(IOException e) {
		System.out.println("Listen :"+e.getMessage());} 
  }
}

/**
 * This inner class handles the receiving files using tcp
 * @author Shabbir
 *
 */
class Connection extends Thread { 
	DataInputStream input; 
	DataOutputStream output; 
	Socket clientSocket; 
	
	String filename = "Received";
	int MaxBytes = 255;
	
	public Connection (Socket aClientSocket) { 
		try { 
			clientSocket = aClientSocket; 
			input = new DataInputStream( clientSocket.getInputStream()); 
			output =new DataOutputStream( clientSocket.getOutputStream()); 
			this.start(); 
		} 
			catch(IOException e) {
			System.out.println("Connection:"+e.getMessage());
			} 
	  } 

	  public void run() { 
		  
		try { 
			  
		   
			  //Step 1 read number of bytes
			  int nb = input.readInt();
			  System.out.println("Read Length: "+ nb);
			  
			  //write to a file
			  FileOutputStream out = new FileOutputStream(filename+nb+"bytes.txt");
			  
			  //Step 2 read byte
			  System.out.println("Writing.......");
			   
			  byte[] readBuf = new byte[MaxBytes];
			  int bytesRECV=0;
			  int bytes=0;
			  while(bytesRECV<nb){
				  bytes=(input.read(readBuf));
				  out.write(readBuf);
				  bytesRECV += bytes;
			  }
			  
			  
			  //send length
			  output.writeInt(bytesRECV); 
			  out.close();
			  
			  System.out.println("Sent Confirmation");
			} 
			catch(EOFException e) {
			System.out.println("EOF:"+e.getMessage()); } 
			catch(IOException e) {
			System.out.println("IO:"+e.getMessage());}  
   
			finally { 
			  try { 
				  clientSocket.close();
			  }
			  catch (IOException e){/*close failed*/}
			}
		}
}

